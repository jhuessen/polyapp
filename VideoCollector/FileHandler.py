'''
Created on Dec 22, 2019

@author: jonas
'''
from xml.dom import minidom 

def readXML(fileName):
    xmldoc = minidom.parse(fileName)
    itemlist = xmldoc.getElementsByTagName('string-array')
    list_of_lists = [];
    for i in range(0, len(itemlist), 2):
        songNames = []
        songLinks = []
        songList = itemlist[i].getElementsByTagName('item')
        linkList = itemlist[i+1].getElementsByTagName('item')
        for j in range(len(songList)):
            songNames.append(songList[j].firstChild.nodeValue)
            songLinks.append(linkList[j].firstChild.nodeValue)
            
        list_of_lists.append(songNames)
        list_of_lists.append(songLinks)
        
    return list_of_lists

def writeXML(fileName, list_of_lists):
    xmldoc = minidom.parse(fileName)
    itemlist = xmldoc.getElementsByTagName('string-array')
    for i in range(0, len(itemlist), 2):
        songLinks = list_of_lists[i+1]
        linkList = itemlist[i+1].getElementsByTagName('item')
        for j in range(len(linkList)):
            newText = "\u003C![CDATA[" + songLinks[j] + "]]\u003E"
            if linkList[j].firstChild.nodeValue == 'Link':
                linkList[j].firstChild.replaceWholeText(newText)
            elif linkList[j].firstChild.nodeValue != songLinks[j]:
                linkList[j].firstChild.replaceWholeText(songLinks[j])
        
    with open(fileName, "w") as xml_file:
        xmldoc.writexml(xml_file)
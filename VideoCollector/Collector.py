'''
Created on Dec 22, 2019

@author: jonas
'''


import urllib.request
import urllib.parse
import re
import FileHandler

list_of_lists = FileHandler.readXML('repertoire.xml')

for i in range(0, len(list_of_lists), 2):
    names = list_of_lists[i]
    links = list_of_lists[i+1]
    for j in range(len(names)):
        print(names[j])
        query_string = urllib.parse.urlencode({"search_query" : names[j]})
        html_content = urllib.request.urlopen("http://www.youtube.com/results?" + query_string)
        search_results = re.findall(r'href=\"\/watch\?v=(.{11})', html_content.read().decode())
        if len(search_results) > 0:
            fullLink = "http://www.youtube.com/watch?v=" + search_results[0]
        
        if fullLink != links[j]:
            links[j] = fullLink
            
            
FileHandler.writeXML('repertoire.xml', list_of_lists)


package publicActivity.videoStuff;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import poly.app.R;
import publicActivity.Config;
import publicActivity.Util;

public class Concert18Fragment extends Fragment implements YouTubePlayer.OnInitializedListener, YouTubePlayer.PlayerStateChangeListener{

    private RecyclerView recyclerView;
    private VideoAdapter adapter;
    private YouTubePlayerSupportFragment youTubePlayerFragment;
    private YouTubePlayer youtubePlayer;
    private String[] videos;
    private String[] titles;
    private int selectedPosition;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.video_18_layout,
                container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        context = view.getContext();

        youTubePlayerFragment = (YouTubePlayerSupportFragment)getChildFragmentManager().findFragmentById(R.id.yt_player_fr_18);
        if(youTubePlayerFragment != null) youTubePlayerFragment.initialize(Config.YOUTUBE_API_KEY, this);

        generateVideoList();

        setUpRecyclerView(view);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        youtubePlayer = player;
        youtubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        youtubePlayer.setPlayerStateChangeListener(this);

        youtubePlayer.cueVideo(videos[0]);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
        Toast.makeText(getContext(), "Initialization failed: " + arg1.toString(), Toast.LENGTH_LONG).show();
    }

    private void setUpRecyclerView(View view) {
        adapter = new VideoAdapter(context, videos, titles);

        recyclerView = view.findViewById(R.id.rec_view_18);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerViewOnClickListener(getContext(), (View mView, int position) -> {
            adapter.setSelectedPosition(position);
            if (youTubePlayerFragment != null && youtubePlayer != null) {
                selectedPosition = position;
                youtubePlayer.cueVideo(videos[position]);
            }
        }));
    }

    private void generateVideoList() {
        videos = getResources().getStringArray(R.array.videos18_id_array);
        titles = getResources().getStringArray(R.array.videos18_titles);
    }

    @Override
    public void onLoading() {

    }

    @Override
    public void onLoaded(String s) {

    }

    @Override
    public void onAdStarted() {

    }

    @Override
    public void onVideoStarted() {

    }

    @Override
    public void onVideoEnded() {
        selectedPosition = (selectedPosition + 1) % videos.length;

        recyclerView.scrollToPosition(selectedPosition);
        adapter.setSelectedPosition(selectedPosition);
        youtubePlayer.loadVideo(videos[selectedPosition]);
    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {
        Toast.makeText(getContext(), "Error: " + errorReason.toString(), Toast.LENGTH_LONG).show();
    }
}

package publicActivity.videoStuff;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import poly.app.R;

class VideoViewHolder extends RecyclerView.ViewHolder{
    TextView videoTitleView;
    CardView videoCardView;

    VideoViewHolder(View view) {
        super(view);
        videoCardView = view.findViewById(R.id.video_card_view);
        videoTitleView = view.findViewById(R.id.video_title_view);
    }
}

package publicActivity.videoStuff;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import poly.app.R;

public class VideoAdapter extends RecyclerView.Adapter<VideoViewHolder> {
    private Context context;
    private String[] videoList;
    private String[] titleList;

    private int bigSize;
    private int smallSize;
    private int padding;

    private int selectedPosition = 0;


    VideoAdapter(Context context, String[] videoList, String[] titleList) {
        this.context = context;
        this.videoList = videoList;
        this.titleList = titleList;

        bigSize = context.getResources().getDimensionPixelSize(R.dimen.video_title_big);
        smallSize = context.getResources().getDimensionPixelSize(R.dimen.video_title_small);
        padding = context.getResources().getDimensionPixelSize(R.dimen.title_padding);
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.video_card_layout, parent, false);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoViewHolder holder, final int position) {

        if (selectedPosition == position) {
            holder.videoCardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.berry));
            ViewGroup.LayoutParams params = holder.videoTitleView.getLayoutParams();
            params.width = smallSize;
            params.height = smallSize;
            holder.videoCardView.setContentPadding(padding, padding, padding, padding);
            holder.videoCardView.setCardElevation(5);
        } else {
            holder.videoCardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.pastel));
            ViewGroup.LayoutParams params = holder.videoTitleView.getLayoutParams();
            params.width = bigSize;
            params.height = bigSize;
            holder.videoCardView.setContentPadding(0, 0, 0, 0);
            holder.videoCardView.setCardElevation(30);
        }

        holder.videoTitleView.setText(titleList[position]);
    }

    @Override
    public int getItemCount() {
        return videoList != null ? videoList.length : 0;
    }

    void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        //when item selected notify the adapter
        notifyDataSetChanged();
    }
}

package publicActivity.videoStuff;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class VideoPagerAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;
    private Fragment[] tabs;

    public VideoPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.tabs = createTabs();
    }

    private Fragment[] createTabs() {
        Fragment[] tabs = new Fragment[mNumOfTabs];

        tabs[0] = new Concert18Fragment();

        return tabs;
    }

    @Override
    public Fragment getItem(int position) {
        return tabs[position];
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

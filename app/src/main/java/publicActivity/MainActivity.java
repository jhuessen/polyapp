package publicActivity;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.Calendar;

import poly.app.R;
import privateActivity.InternalActivity;
import publicActivity.fragments.*;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
    private int activeItemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Toolbar mToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        findViewById(R.id.banner_view).setOnClickListener((View view) -> {
            String url = "http://www.polyphoniker.de";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        });

        if(savedInstanceState == null) {
            setActiveFragment(new AboutUsFragment());
            activeItemId = R.id.nav_about_us;
            navigationView.setCheckedItem(R.id.nav_about_us);
        }
    }

    public void initNotification() {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(MainActivity.this, AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, 3);
        calendar.set(Calendar.HOUR_OF_DAY, 19);

        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                alarmMgr.INTERVAL_DAY*7, alarmIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int color = R.color.berry;
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_logo);
        ActivityManager.TaskDescription description = new ActivityManager.TaskDescription("Polyphoniker", bm, getResources().getColor(color, getTheme()));
        setTaskDescription(description);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish();
            }else {
                super.onBackPressed();
            }
        }
    }

    public void SetNavItemChecked(int id) {
        navigationView.setCheckedItem(id);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            closeKeyboard();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // if already on the selected page
        if(activeItemId == menuItem.getItemId()){
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }

        menuItem.setChecked(true);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        activeItemId = menuItem.getItemId();
        switch(menuItem.getItemId()) {
            case R.id.nav_about_us:
                setActiveFragment(new AboutUsFragment());
                break;
            case R.id.nav_concerts:
                setActiveFragment(new ConcertsFragment());
                break;
            case R.id.nav_repertoire:
                setActiveFragment(new RepertoireFragment());
                break;
            case R.id.nav_sing_along:
                setActiveFragment(new SingAlongFragment());
                break;
            case R.id.nav_photos:
                setActiveFragment(new PhotosFragment());
                break;
            case R.id.nav_videos:
                setActiveFragment(new VideosFragment());
                break;
            case R.id.nav_impressum:
                setActiveFragment(new ImpressumFragment());
                break;
            case R.id.nav_int_area:
                if(Config.signedIn){
                    Intent intent = new Intent(this, InternalActivity.class);
                    startActivity(intent);
                }else {
                    setActiveFragment(new IntAreaFragment());
                }
                break;
        }

        return true;
    }

    private void setActiveFragment(Fragment fragment) {
        new Thread(() -> {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mContent, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }).start();
    }
}

package publicActivity.repertoireStuff;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import poly.app.R;

public class RepertoirePagerAdapter extends FragmentStatePagerAdapter {
    private Fragment[] tabs;

    public RepertoirePagerAdapter(FragmentManager fm) {
        super(fm);
        this.tabs = createTabs();
    }

    private Fragment[] createTabs() {
        Fragment[] tabs = new Fragment[10];

        tabs[0] = createFragment(R.array.klassik_songs, R.array.klassik_links);
        tabs[1] = createFragment(R.array.renaissance_songs, R.array.renaissance_links);
        tabs[2] = createFragment(R.array.romantik_songs, R.array.romantik_links);
        tabs[3] = createFragment(R.array.comedian_songs, R.array.comedian_links);
        tabs[4] = createFragment(R.array.volkslied_de_songs, R.array.volkslied_de_links);
        tabs[5] = createFragment(R.array.filmmusik_songs, R.array.filmmusik_links);
        tabs[6] = createFragment(R.array.volkslied_songs, R.array.volkslied_links);
        tabs[7] = createFragment(R.array.musical_songs, R.array.musical_links);
        tabs[8] = createFragment(R.array.rock_pop_songs, R.array.rock_pop_links);
        tabs[9] = createFragment(R.array.weihnachten_songs, R.array.weihnachten_links);

        return tabs;
    }

    private Fragment createFragment(int song_titles, int song_links) {
        GenreFragment fragment = new GenreFragment();
        fragment.setTitles(song_titles);
        fragment.setLinks(song_links);
        return fragment;
    }

    @Override
    public Fragment getItem(int position) {
        return tabs[position];
    }

    @Override
    public int getCount() {
        return tabs.length;
    }
}

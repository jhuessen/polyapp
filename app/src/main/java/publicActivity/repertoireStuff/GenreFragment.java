package publicActivity.repertoireStuff;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TableLayout;

import poly.app.R;

import static publicActivity.fragments.RepertoireFragment.populateTableLayout;

public class GenreFragment extends Fragment {

    private int song_titles;
    private int song_links;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.genre_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        TableLayout mTable = new TableLayout(getContext());
        mTable.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                            ViewGroup.LayoutParams.WRAP_CONTENT));
        mTable.setStretchAllColumns(true);
        String[] songs = getResources().getStringArray(song_titles);
        String[] links = getResources().getStringArray(song_links);
        populateTableLayout(mTable, songs, links);


        ScrollView v = (ScrollView) view;
        v.addView(mTable);
    }

    public void setTitles(int song_titles){
        this.song_titles = song_titles;
    }

    public void setLinks(int song_links){
        this.song_links = song_links;
    }

}

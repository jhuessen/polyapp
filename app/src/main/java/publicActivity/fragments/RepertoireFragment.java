package publicActivity.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import publicActivity.MainActivity;
import poly.app.R;
import publicActivity.repertoireStuff.RepertoirePagerAdapter;

public class RepertoireFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.repertoire_layout,
                container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        TabLayout mTabs = view.findViewById(R.id.rep_tab_layout);
        mTabs.addTab(mTabs.newTab().setText("Chormusik Klassik"));
        mTabs.addTab(mTabs.newTab().setText("Chormusik Renaissance"));
        mTabs.addTab(mTabs.newTab().setText("Chormusik Romantik"));
        mTabs.addTab(mTabs.newTab().setText("Comedian Harmonics & Co."));
        mTabs.addTab(mTabs.newTab().setText("Deutsches Volkslied"));
        mTabs.addTab(mTabs.newTab().setText("Filmmusik"));
        mTabs.addTab(mTabs.newTab().setText("Internationales Volkslied"));
        mTabs.addTab(mTabs.newTab().setText("Musical"));
        mTabs.addTab(mTabs.newTab().setText("Rock/Pop"));
        mTabs.addTab(mTabs.newTab().setText("Weihnachten"));

        mTabs.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabs.setTabMode(TabLayout.MODE_SCROLLABLE);

        final ViewPager vp = view.findViewById(R.id.rep_pager);
        final RepertoirePagerAdapter adapter = new RepertoirePagerAdapter(getChildFragmentManager());

        vp.setAdapter(adapter);
        vp.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabs));
        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity)getActivity()).SetNavItemChecked(R.id.nav_repertoire);
    }

    public static void populateTableLayout(TableLayout mTable, String[] songs, String[] links){
        int c;
        for(int i = 0; i < songs.length; i++){
            if(i % 2 == 0) {
                c = Color.WHITE;
            }else{
                c = ContextCompat.getColor(mTable.getContext(), R.color.pastel);
            }
            createRow(mTable, songs[i], links[i], c);
        }
    }

    public static void createRow(TableLayout mTable, String song, String link, int c){
        TableRow tr = new TableRow(mTable.getContext());
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(lp);
        tr.setBackgroundColor(c);

        TextView tvSong = new TextView(mTable.getContext());
        TableRow.LayoutParams songParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        songParams.setMarginStart(20);
        songParams.setMargins(0,7, 0, 0);
        tvSong.setLayoutParams(songParams);
        tvSong.setText(song);
        tvSong.setTextColor(Color.parseColor("#000000"));
        tvSong.setTextSize(15);

        TextView tvLink = new TextView(mTable.getContext());
        TableRow.LayoutParams linkParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        linkParams.setMarginEnd(30);
        linkParams.setMargins(0,5, 0, 0);
        tvLink.setLayoutParams(linkParams);
        tvLink.setTextColor(Color.parseColor("#000000"));
        tvLink.setTextSize(15);
        tvLink.setGravity(Gravity.END);

        SpannableString linkString = new SpannableString("Youtube");
        linkString.setSpan(new URLSpan(link), 0, linkString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvLink.setText(linkString);
        tvLink.setMovementMethod(LinkMovementMethod.getInstance());

        tr.addView(tvSong);
        tr.addView(tvLink);
        mTable.addView(tr);
    }
}

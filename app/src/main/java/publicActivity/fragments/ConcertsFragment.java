package publicActivity.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import publicActivity.MainActivity;
import poly.app.R;

public class ConcertsFragment extends Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.concerts_layout,
                container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        TableLayout mTable = view.findViewById(R.id.concerts_table);

        String[] dates = getResources().getStringArray(R.array.concert_dates);
        String[] descs = getResources().getStringArray(R.array.concert_descriptions);
        String[] places = getResources().getStringArray(R.array.concert_places);


        for(int i = 0; i < dates.length; i++) {
            addConcert(mTable, dates[i], descs[i], places[i]);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).SetNavItemChecked(R.id.nav_concerts);
    }

    private void addConcert(TableLayout mTable, String date, String description, String place){
        TableRow tr = new TableRow(mTable.getContext());
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
        tr.setLayoutParams(lp);

        TextView tvDate = new TextView(mTable.getContext());
        TableRow.LayoutParams dateParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        dateParams.setMarginStart(30);
        dateParams.setMargins(0, 15, 0, 15);
        tvDate.setLayoutParams(dateParams);
        tvDate.setText(date);
        tvDate.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        tvDate.setTextSize(17);

        TextView tvPlace = new TextView(mTable.getContext());
        TableRow.LayoutParams placeParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        placeParams.setMarginStart(30);
        placeParams.setMarginEnd(10);
        placeParams.setMargins(0, 15, 0, 15);
        placeParams.weight = 1;

        tvPlace.setLayoutParams(placeParams);
        tvPlace.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        tvPlace.setTextSize(17);
        tvPlace.setLinkTextColor(ContextCompat.getColor(getContext(), R.color.linkBlue));

        SpannableString linkString = new SpannableString(description);
        linkString.setSpan(new URLSpan(place), 0, description.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvPlace.setMovementMethod(LinkMovementMethod.getInstance());
        tvPlace.setText(linkString);

        tr.addView(tvDate);
        tr.addView(tvPlace);
        mTable.addView(tr);
    }

}

package publicActivity.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import publicActivity.MainActivity;
import poly.app.R;

public class ImpressumFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.impressum_layout,
                container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity)getActivity()).SetNavItemChecked(R.id.nav_impressum);
    }

}

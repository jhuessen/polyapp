package publicActivity.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.URL;

import publicActivity.MainActivity;
import poly.app.R;

public class PhotosFragment extends Fragment {

    String[] im_urls;
    ImageView[] imageViews;
    ProgressBar[] progressBars;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater
                .inflate(R.layout.photos_layout, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        populate(view);
    }

    private void populate(View view) {
        LinearLayout ll = view.findViewById(R.id.photos_layout);
        im_urls = getResources().getStringArray(R.array.photo_urls);
        String[] im_descs = getResources().getStringArray(R.array.photo_descs);
        imageViews = new ImageView[im_urls.length];
        progressBars = new ProgressBar[im_urls.length];
        SimpleDB.successfulDownload = new boolean[im_urls.length];

        if(SimpleDB.savedPhotos == null) {
            SimpleDB.savedPhotos = new Bitmap[im_urls.length];
        }
        for(int i = 0; i < im_urls.length; i++) {

            ImageView iv = new ImageView(getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            iv.setLayoutParams(params);
            iv.setAdjustViewBounds(true);
            TextView tv = new TextView(getContext());
            tv.setGravity(Gravity.CENTER_HORIZONTAL);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            tv.setTextColor(ContextCompat.getColor(getContext(), R.color.black));

            ProgressBar pb = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleSmall);

            ll.addView(pb);
            ll.addView(iv);
            ll.addView(tv);
            imageViews[i] = iv;
            progressBars[i] = pb;

            if(SimpleDB.savedPhotos[i] != null) {
                pb.setVisibility(View.GONE);
                iv.setImageBitmap(SimpleDB.savedPhotos[i]);
            }else{
                try {
                    new DownLoadImageTask(iv, i, pb).execute(im_urls[i]);
                }catch(Exception e) {
                    Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            String im_desc = im_descs[i];
            tv.setText(im_desc);
            final int ci = i;
            iv.setOnClickListener((View v) -> {
                String url = im_urls[ci];
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            });
        }
    }

    private void downloadMissingImages() {
        SimpleDB.finishedTasks = 0;
        for(int i = 0; i < SimpleDB.successfulDownload.length; i++) {
            if(!SimpleDB.successfulDownload[i]) {
                imageViews[i].setImageDrawable(null);
                progressBars[i].setVisibility(View.VISIBLE);
                try {
                    new DownLoadImageTask(imageViews[i], i, progressBars[i]).execute(im_urls[i]);
                }catch(Exception e) {
                    Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void checkDownloads() {
        boolean success = true;
        for(int i = 0; i < SimpleDB.successfulDownload.length; i++) {
            if(!SimpleDB.successfulDownload[i]) {
                success = false;
                break;
            }
        }
        if(!success) {
            View view = this.getView();
            if(view != null) {
                Snackbar.make(this.getView(), "Failed to Download Images", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Retry", (View v) -> downloadMissingImages())
                        .show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity)getActivity()).SetNavItemChecked(R.id.nav_photos);
    }

    private class DownLoadImageTask extends AsyncTask<String,Void,Bitmap>{
        ImageView imageView;
        int index;
        ProgressBar pb;

        DownLoadImageTask(ImageView imageView, int index, ProgressBar pb){
            this.index = index;
            this.imageView = imageView;
            this.pb = pb;
        }

        protected Bitmap doInBackground(String...urls){
            String urlOfImage = urls[0];
            Bitmap image = null;
            try{
                InputStream is = new URL(urlOfImage).openStream();
                image = BitmapFactory.decodeStream(is);
            }catch(Exception e){
                e.printStackTrace();
            }
            SimpleDB.savedPhotos[index] = image;
            return image;
        }

        protected void onPostExecute(Bitmap result){
            pb.setVisibility(View.GONE);
            SimpleDB.finishedTasks++;
            if(result != null) {
                SimpleDB.successfulDownload[index] = true;
                imageView.setImageBitmap(result);
            }else {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.no_internet, getContext().getTheme()));
            }
            if(SimpleDB.finishedTasks == SimpleDB.successfulDownload.length) {
                checkDownloads();
            }
        }
    }




}

package publicActivity.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import privateActivity.InternalActivity;
import publicActivity.Config;
import publicActivity.MainActivity;
import poly.app.R;

public class IntAreaFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    private EditText passwordText;
    private TextView errorText;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.intarea_layout,
                container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        passwordText = view.findViewById(R.id.pw_text);
        errorText = view.findViewById(R.id.pw_error);

        AppCompatCheckBox pw_checkbox = view.findViewById(R.id.pw_checkbox);
        pw_checkbox.setOnCheckedChangeListener(this);

        Button loginButton = view.findViewById(R.id.login_button);
        loginButton.setOnClickListener((View v) -> {
            checkPassword(passwordText.getText().toString());
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity)getActivity()).SetNavItemChecked(R.id.nav_int_area);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (isChecked) {
            passwordText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            passwordText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    private void checkPassword(String pw){
        if(pw.hashCode() == Config.PASSWORD){
            Config.signedIn = true;
            Intent intent = new Intent(getActivity(), InternalActivity.class);
            startActivity(intent);
        }else{
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            errorText.setVisibility(View.VISIBLE);
        }
    }

}

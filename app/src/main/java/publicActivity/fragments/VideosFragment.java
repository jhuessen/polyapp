package publicActivity.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import publicActivity.MainActivity;
import poly.app.R;
import publicActivity.videoStuff.VideoPagerAdapter;


public class VideosFragment extends Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.videos_layout,
                container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        TabLayout mTabs = view.findViewById(R.id.video_tab_layout);
        mTabs.addTab(mTabs.newTab().setText("Konzerte 2018"));
        mTabs.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabs.setTabMode(TabLayout.MODE_SCROLLABLE);

        final ViewPager vp = view.findViewById(R.id.video_pager);
        final VideoPagerAdapter adapter = new VideoPagerAdapter(getChildFragmentManager(), mTabs.getTabCount());

        vp.setAdapter(adapter);
        vp.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabs));
        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity)getActivity()).SetNavItemChecked(R.id.nav_videos);
    }

}

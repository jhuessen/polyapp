package privateActivity.midiStuff;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import privateActivity.fragments.MidiFragment;


public class MidiPagerAdapter extends FragmentStatePagerAdapter {
    private Fragment[] tabs;
    private MidiUIAdapter adapter;

    public MidiPagerAdapter(FragmentManager fm, MidiUIAdapter adapter) {
        super(fm);
        this.adapter = adapter;
        this.tabs = createTabs();
    }

    private Fragment[] createTabs() {
        Fragment[] tabs = new Fragment[5];

        tabs[0] = createFragment(MidiDB.ids_all, MidiDB.names_all, 0);
        tabs[1] = createFragment(MidiDB.ids_soprano, MidiDB.names_soprano, 1);
        tabs[2] = createFragment(MidiDB.ids_alto, MidiDB.names_alto, 2);
        tabs[3] = createFragment(MidiDB.ids_tenor, MidiDB.names_tenor, 3);
        tabs[4] = createFragment(MidiDB.ids_bass, MidiDB.names_bass, 4);

        return tabs;
    }

    private Fragment createFragment(Integer[] ids, String[] titles, int voice) {
        VoiceFragment fragment = new VoiceFragment();
        fragment.setFields(adapter, ids, titles, voice);
        return fragment;
    }

    @Override
    public Fragment getItem(int position) {
        return tabs[position];
    }

    @Override
    public int getCount() {
        return tabs.length;
    }
}

package privateActivity.midiStuff;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import poly.app.R;

public class VoiceFragment extends Fragment {

    TableLayout mTable;
    MidiUIAdapter uiAdapter;
    Integer[] ids;
    String[] titles;
    int voice;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.voice_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        populateTableLayout();

        ScrollView v = (ScrollView) view;
        v.addView(mTable);
    }

    public void setFields(MidiUIAdapter adapter, Integer[] ids, String[] titles, int voice) {
        this.uiAdapter = adapter;
        this.ids = ids;
        this.titles = titles;
        this.voice = voice;
    }

    public void populateTableLayout(){
        mTable = new TableLayout(getContext());
        mTable.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mTable.setStretchAllColumns(true);
        int c;
        for(int i = 0; i < ids.length; i++){
            if(i % 2 == 0) {
                c = Color.WHITE;
            }else{
                c = ContextCompat.getColor(getContext(), R.color.pastel);
            }
            createRow(uiAdapter, new MidiContainer(ids[i], titles[i], voice), c);
        }
    }

    private void createRow(MidiUIAdapter adapter, MidiContainer midi, int c){
        TableRow tr = new TableRow(getContext());
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(lp);
        tr.setBackgroundColor(c);
        tr.setClickable(true);

        TextView tvTitle = new TextView(getContext());
        TableRow.LayoutParams songParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        songParams.setMarginStart(20);
        songParams.setMargins(0,10, 0, 10);
        songParams.gravity = Gravity.CENTER;
        tvTitle.setLayoutParams(songParams);
        tvTitle.setText(midi.getTitle());
        tvTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        tvTitle.setTextSize(18);

        tr.setOnClickListener((View view) -> adapter.notifySongChanged(midi));

        tr.addView(tvTitle);

        mTable.addView(tr);
    }

}

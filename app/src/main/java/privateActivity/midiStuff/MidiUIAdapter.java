package privateActivity.midiStuff;

import privateActivity.fragments.MidiFragment;

public class MidiUIAdapter {

    private MidiFragment midiFragment;

    public MidiUIAdapter(MidiFragment midiFragment) {
        this.midiFragment = midiFragment;
    }

    void notifySongChanged(MidiContainer midi) {
        midiFragment.changeSong(midi.getId(), midi.getTitle(), midi.getVoice());
    }

}

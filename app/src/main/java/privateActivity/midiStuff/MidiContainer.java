package privateActivity.midiStuff;

class MidiContainer {

    private int id;
    private String title;
    private int voice;

    MidiContainer(int id, String title, int voice) {
        this.id = id;
        this.title = title;
        this.voice = voice;
    }

    int getId() {
        return id;
    }

    String getTitle() {
        return title;
    }

    int getVoice() {
        return voice;
    }
}

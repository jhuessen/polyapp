package privateActivity.midiStuff;

import android.media.MediaPlayer;

import java.util.ArrayList;
import java.util.List;

public class MidiDB {

    public static Integer[] ids_all;
    public static Integer[] ids_soprano;
    public static Integer[] ids_alto;
    public static Integer[] ids_tenor;
    public static Integer[] ids_bass;

    public static String[] names_all;
    public static String[] names_soprano;
    public static String[] names_alto;
    public static String[] names_tenor;
    public static String[] names_bass;

}

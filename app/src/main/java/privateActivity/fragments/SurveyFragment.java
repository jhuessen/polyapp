package privateActivity.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import poly.app.R;
import privateActivity.InternalActivity;

public class SurveyFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.survey_layout,
                container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        populate(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((InternalActivity)getActivity()).SetNavItemChecked(R.id.nav_survey);
    }

    private void populate(View view) {
        LinearLayout content_layout = view.findViewById(R.id.survey_content);
        String[] survey_titles = getResources().getStringArray(R.array.survey_titles);
        String[] survey_links = getResources().getStringArray(R.array.survey_links);

        for(int i = 0; i < survey_titles.length; i++) {
            TextView tv = new TextView(getContext());
            tv.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            tv.setLinkTextColor(ContextCompat.getColor(getContext(), R.color.linkBlue));
            tv.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/comic_sans.ttf"));
            tv.setGravity(Gravity.CENTER);
            tv.setBackgroundResource(R.drawable.content_item_bg);
            tv.setPadding(20, 20, 20, 20);
            tv.setElevation(15);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 30);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            tv.setLayoutParams(params);

            SpannableString linkString = new SpannableString(survey_titles[i]);
            linkString.setSpan(new URLSpan(survey_links[i]), 0, survey_titles[i].length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv.setMovementMethod(LinkMovementMethod.getInstance());
            tv.setText(linkString);

            content_layout.addView(tv);
        }

    }

}

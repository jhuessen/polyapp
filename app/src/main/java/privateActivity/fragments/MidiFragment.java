package privateActivity.fragments;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import poly.app.R;
import privateActivity.InternalActivity;
import privateActivity.midiStuff.MidiDB;
import privateActivity.midiStuff.MidiPagerAdapter;
import privateActivity.midiStuff.MidiUIAdapter;

public class MidiFragment extends Fragment {

    private Button playButton;
    private TextView titleText, durationText, playedText,
                        sopranoText, altoText, tenorText, bassText;
    private SeekBar seekBar;

    private MediaPlayer mp;

    private Handler handler = new Handler();
    private int currentId = 0;
    private double elapsedTime = 0;
    private double duration = 0;

    private Runnable updateUI = new Runnable() {
        @Override
        public void run() {
            elapsedTime = mp.getCurrentPosition();
            playedText.setText(time2String(elapsedTime));
            seekBar.setProgress((int) elapsedTime);

            handler.postDelayed(this, 100);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.midi_layout,
                container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        collectMidis();
        playButton = view.findViewById(R.id.midi_play_button);
        titleText = view.findViewById(R.id.midi_title_text);
        durationText = view.findViewById(R.id.midi_duration_text);
        playedText = view.findViewById(R.id.midi_played_text);
        seekBar = view.findViewById(R.id.midi_seek_bar);

        sopranoText = view.findViewById(R.id.midi_soprano_view);
        altoText = view.findViewById(R.id.midi_alto_view);
        tenorText = view.findViewById(R.id.midi_tenor_view);
        bassText = view.findViewById(R.id.midi_bass_view);

        MidiUIAdapter uiAdapter = new MidiUIAdapter(this);

        mp = MediaPlayer.create(getContext(), MidiDB.ids_all[0]);
        changeSong(MidiDB.ids_all[0], MidiDB.names_all[0], 0);
        pause();

        bindUI();

        TabLayout mTabs = view.findViewById(R.id.midis_tab_layout);
        mTabs.addTab(mTabs.newTab().setText("Alle"));
        mTabs.addTab(mTabs.newTab().setText("Sopran"));
        mTabs.addTab(mTabs.newTab().setText("Alt"));
        mTabs.addTab(mTabs.newTab().setText("Tenor"));
        mTabs.addTab(mTabs.newTab().setText("Bass"));

        mTabs.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabs.setTabMode(TabLayout.MODE_SCROLLABLE);

        final ViewPager vp = view.findViewById(R.id.midi_pager);
        final MidiPagerAdapter pagerAdapter = new MidiPagerAdapter(getChildFragmentManager(), uiAdapter);

        vp.setAdapter(pagerAdapter);
        vp.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabs));
        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        handler.postDelayed(updateUI, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((InternalActivity)getActivity()).SetNavItemChecked(R.id.nav_midi);
        playButton.setBackgroundResource(R.drawable.play_circle);
        durationText.setText(time2String(duration));
        mp = MediaPlayer.create(getContext(), currentId);
        mp.seekTo((int)elapsedTime);
        handler.postDelayed(updateUI, 100);
    }

    @Override
    public void onPause() {
        super.onPause();
        pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        stop();
    }

    public void stop() {
        mp.stop();
        mp.reset();
        mp.release();
        handler.removeCallbacks(updateUI);
    }

    public void pause() {
        mp.pause();
        playButton.setBackgroundResource(R.drawable.play_circle);
        handler.removeCallbacks(updateUI);
    }

    public void play() {
        mp.start();
        playButton.setBackgroundResource(R.drawable.pause_circle);
        handler.postDelayed(updateUI, 100);
    }

    private String time2String(double timeMillis) {
        int minutes = (int)TimeUnit.MILLISECONDS.toMinutes((long)timeMillis);
        int seconds = (int)(TimeUnit.MILLISECONDS.toSeconds((long)timeMillis)-
                            TimeUnit.MINUTES.toSeconds((long)minutes));
        return String.format(Locale.ENGLISH, "%d:%02d",
                minutes, seconds);
    }

    public void bindUI() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser) {
                    mp.seekTo(progress);
                    elapsedTime = mp.getCurrentPosition();
                    playedText.setText(time2String(elapsedTime));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        playButton.setOnClickListener((View view) -> {
            if(mp.isPlaying()) {
                pause();
            }else {
                play();
            }
        });

    }

    public void changeSong(int id, String title, int voice) {
        stop();
        mp = MediaPlayer.create(getContext(), id);
        currentId = id;
        duration = mp.getDuration();
        seekBar.setMax((int)duration);
        titleText.setText(title);
        durationText.setText(time2String(duration));
        elapsedTime = mp.getCurrentPosition();
        playedText.setText(time2String(elapsedTime));
        seekBar.setProgress((int) elapsedTime);
        if(mp.isPlaying()) {
            playButton.setBackgroundResource(R.drawable.pause_circle);
        }else {
            playButton.setBackgroundResource(R.drawable.play_circle);
        }
        markVoice(voice);
        play();
        handler.postDelayed(updateUI, 100);
    }

    public void markVoice(int voice) {
        switch(voice) {
            case 0:
                sopranoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.berry));
                sopranoText.setTextColor(ContextCompat.getColor(getContext(), R.color.pastel));
                sopranoText.setElevation(0);
                altoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.berry));
                altoText.setTextColor(ContextCompat.getColor(getContext(), R.color.pastel));
                altoText.setElevation(0);
                tenorText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.berry));
                tenorText.setTextColor(ContextCompat.getColor(getContext(), R.color.pastel));
                tenorText.setElevation(0);
                bassText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.berry));
                bassText.setTextColor(ContextCompat.getColor(getContext(), R.color.pastel));
                bassText.setElevation(0);
                break;
            case 1:
                sopranoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.berry));
                sopranoText.setTextColor(ContextCompat.getColor(getContext(), R.color.pastel));
                sopranoText.setElevation(0);
                altoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                altoText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                altoText.setElevation(5);
                tenorText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                tenorText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                tenorText.setElevation(5);
                bassText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                bassText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                bassText.setElevation(5);
                break;
            case 2:
                sopranoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                sopranoText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                sopranoText.setElevation(5);
                altoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.berry));
                altoText.setTextColor(ContextCompat.getColor(getContext(), R.color.pastel));
                altoText.setElevation(0);
                tenorText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                tenorText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                tenorText.setElevation(5);
                bassText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                bassText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                bassText.setElevation(5);
                break;
            case 3:
                sopranoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                sopranoText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                sopranoText.setElevation(5);
                altoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                altoText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                altoText.setElevation(5);
                tenorText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.berry));
                tenorText.setTextColor(ContextCompat.getColor(getContext(), R.color.pastel));
                tenorText.setElevation(0);
                bassText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                bassText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                bassText.setElevation(5);
                break;
            case 4:
                sopranoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                sopranoText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                sopranoText.setElevation(5);
                altoText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                altoText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                altoText.setElevation(5);
                tenorText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.pastel));
                tenorText.setTextColor(ContextCompat.getColor(getContext(), R.color.berry));
                tenorText.setElevation(5);
                bassText.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.berry));
                bassText.setTextColor(ContextCompat.getColor(getContext(), R.color.pastel));
                bassText.setElevation(0);
                break;

        }
    }

    public void collectMidis() {
        List<Integer> id_all = new ArrayList<>();
        List<Integer> id_soprano = new ArrayList<>();
        List<Integer> id_alto = new ArrayList<>();
        List<Integer> id_tenor = new ArrayList<>();
        List<Integer> id_bass = new ArrayList<>();

        List<String> name_all = new ArrayList<>();
        List<String> name_soprano = new ArrayList<>();
        List<String> name_alto = new ArrayList<>();
        List<String> name_tenor = new ArrayList<>();
        List<String> name_bass = new ArrayList<>();

        Field[] midis = R.raw.class.getFields();

        Map<String, String> titleMap = createTitleMap();

        for(Field f : midis) {
            String[] name = f.getName().split("_");
            String titleKey = name[0];
            String voiceKey = name[1];
            String title = titleMap.get(titleKey);
            int id = 0;
            try {
                id = f.getInt(f);
            }catch(Exception e) {
                Log.i("couldnt get id of ", titleKey + " " + voiceKey);
            }
            if(voiceKey.contains("alle")) {
                name_all.add(title);
                id_all.add(id);
            }else if(voiceKey.contains("sopran")) {
                if(voiceKey.contains("1")) {
                    name_soprano.add(title + " S1");
                }else if(voiceKey.contains("2")) {
                    name_soprano.add(title + " S2");
                }else {
                    name_soprano.add(title);
                }
                id_soprano.add(id);
            }else if(voiceKey.contains("alt")) {
                if(voiceKey.contains("1")) {
                    name_alto.add(title + " A1");
                }else if(voiceKey.contains("2")) {
                    name_alto.add(title + " A2");
                }else {
                    name_alto.add(title);
                }
                id_alto.add(id);
            }else if(voiceKey.contains("tenor")) {
                if(voiceKey.contains("1")) {
                    name_tenor.add(title + " T1");
                }else if(voiceKey.contains("2")) {
                    name_tenor.add(title + " T2");
                }else {
                    name_tenor.add(title);
                }
                id_tenor.add(id);
            }else if(voiceKey.contains("bass")) {
                if(voiceKey.contains("1")) {
                    name_bass.add(title + " B1");
                }else if(voiceKey.contains("2")) {
                    name_bass.add(title + " B2");
                }else {
                    name_bass.add(title);
                }
                id_bass.add(id);
            }
        }

        MidiDB.ids_all = id_all.toArray(new Integer[0]);
        MidiDB.ids_soprano = id_soprano.toArray(new Integer[0]);
        MidiDB.ids_alto = id_alto.toArray(new Integer[0]);
        MidiDB.ids_tenor = id_tenor.toArray(new Integer[0]);
        MidiDB.ids_bass = id_bass.toArray(new Integer[0]);

        MidiDB.names_all = name_all.toArray(new String[0]);
        MidiDB.names_soprano = name_soprano.toArray(new String[0]);
        MidiDB.names_alto = name_alto.toArray(new String[0]);
        MidiDB.names_tenor = name_tenor.toArray(new String[0]);
        MidiDB.names_bass = name_bass.toArray(new String[0]);

    }

    public Map<String, String> createTitleMap() {
        Map<String, String> map = new HashMap<>();

        String[] keys = getResources().getStringArray(R.array.midi_keys);
        String[] values = getResources().getStringArray(R.array.midi_values);

        for(int i = 0; i < keys.length; i++) {
            map.put(keys[i], values[i]);
        }

        return map;
    }




}

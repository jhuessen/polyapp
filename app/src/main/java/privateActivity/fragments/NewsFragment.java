package privateActivity.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import poly.app.R;
import privateActivity.InternalActivity;

public class NewsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news_layout,
                container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        populate(view);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((InternalActivity)getActivity()).SetNavItemChecked(R.id.nav_news);
    }

    private void populate(View view) {
        LinearLayout content_layout = view.findViewById(R.id.news_content);
        String[] news = getResources().getStringArray(R.array.news);

        for(int i = 0; i < news.length; i++) {
            TextView tv = new TextView(getContext());
            tv.setText(news[i]);
            tv.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            tv.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/comic_sans.ttf"));
            tv.setGravity(Gravity.CENTER);
            tv.setBackgroundResource(R.drawable.content_item_bg);
            tv.setPadding(10, 10, 10, 10);
            tv.setElevation(15);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 30);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            tv.setLayoutParams(params);

            content_layout.addView(tv);
        }

    }

}

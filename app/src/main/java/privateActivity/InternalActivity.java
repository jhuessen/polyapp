package privateActivity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import poly.app.R;
import privateActivity.fragments.DatesFragment;
import privateActivity.fragments.MidiFragment;
import privateActivity.fragments.NewsFragment;
import privateActivity.fragments.ResourceFragment;
import privateActivity.fragments.SurveyFragment;
import publicActivity.MainActivity;

public class InternalActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
    private int activeItemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().clear();
        navigationView.inflateMenu(R.menu.drawer_layout_internal);
        navigationView.setNavigationItemSelectedListener(this);

        Toolbar mToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        findViewById(R.id.banner_view).setOnClickListener((View view) -> {
            String url = "http://www.polyphoniker.de";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        });

        if(savedInstanceState == null) {
            setActiveFragment(new NewsFragment());
            activeItemId = R.id.nav_news;
            navigationView.setCheckedItem(R.id.nav_news);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        int color = R.color.berry;
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_logo);
        ActivityManager.TaskDescription description = new ActivityManager.TaskDescription("Polyphoniker", bm, getResources().getColor(color, getTheme()));
        setTaskDescription(description);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(navigationView.getCheckedItem().getItemId() != R.id.nav_news) {
                super.onBackPressed();
            }
        }
    }

    public void SetNavItemChecked(int id) {
        navigationView.setCheckedItem(id);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            closeKeyboard();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // if already on the selected page
        if(activeItemId == menuItem.getItemId()){
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }

        menuItem.setChecked(true);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        activeItemId = menuItem.getItemId();
        switch(menuItem.getItemId()) {
            case R.id.nav_news:
                setActiveFragment(new NewsFragment());
                break;
            case R.id.nav_dates:
                setActiveFragment(new DatesFragment());
                break;
            case R.id.nav_midi:
                setActiveFragment(new MidiFragment());
                break;
            case R.id.nav_survey:
                setActiveFragment(new SurveyFragment());
                break;
            case R.id.nav_resource:
                setActiveFragment(new ResourceFragment());
                break;
            case R.id.nav_leave:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }

    private void setActiveFragment(Fragment fragment) {
        new Thread(() -> {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mContent, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }).start();
    }
}
